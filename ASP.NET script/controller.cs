using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
// Tilføjer for at kunne bruge nedenstående kode ( SQL data og Models for at kunne bruge vores modeller )
using WebASP.Models;
using System.Data.SqlClient;

namespace WebASP.Controllers
{
    // Nedarver fra Controller 
    public class MoviesController : Controller
    {
        // index = vores film liste side   local/Movies.
        public IActionResult index()
        {
            // Sætter Results til at være vores list metode
            ViewData["Results"] = list(); return View();

        }
        // Create = vores create af ny film.  local/Movies/Create
        public IActionResult Create()
        {
            return View();

        }
        // SaveData = bruger vi til at indsætte den nye data i databasen ( efter Create )
        public IActionResult SaveData(Create ct)
        {
            // Variabler             
            string navn = ct.Instruktør_navn;
            int Å = ct.Årstal;
            string FT = ct.Film_titel;
            bool Try = true;
            string id = "";
            int finalID = 0;

            // sætter sql connection til at være Con og fortæller connectionstring.
            SqlConnection Con;
            Con = new SqlConnection("Data Source=DESKTOPN0FHQK4; Initial Catalog = LAX; Integrated Security = True");
            // Læser id fra instruktør. vi gemmer så den sidste id for der ikke er nogen film med den samme id ( og hvert id er unique)
            SqlCommand Ok1 = new SqlCommand("SELECT id FROM instruktør", Con)
;
            try
            {
                Con.Open();
                using (SqlDataReader read = Ok1.ExecuteReader())
                {
                    while (read.Read())
                    {
                        id = (read["id"].ToString());
                    }
                }
                int masterID = Convert.ToInt32(id);
                finalID = masterID + 1;
                Con.Close();
            }
            catch (Exception IDfejl)
            {
                finalID = 1;
                Con.Close();
            }

            try
            {
                Con.Open();                 //Indsætter i slq.
                SqlCommand Ok = new SqlCommand("INSERT INTO instruktør(id, Instruktør_navn) VALUES('" + finalID + "', '" + navn + "')", Con);
                Ok.ExecuteNonQuery();

                SqlCommand Ok2 = new SqlCommand("INSERT INTO film(id, Film_ti tel,Årstal) VALUES('" + finalID + "', '" + FT + "', '" + Å + "')" + "", Con);
                Ok2.ExecuteNonQuery();

                Try = false;

            }

            catch (Exception ImputFailure)
            {

                Try = true;
            }

            if (Try == false)
            {

            }
            return RedirectToAction("index");
        }

        // Delete = Slet, vi gør vi kan få værdierne fra vores Movie og Delet e modeller.
        public IActionResult Delete(Movie values, Delete values2)
        {
            //Sætter id = id fra hver deres model, bruger vi til at kunne slet og indentificere hver enkelte film.  local/Movies/Delete/id
            values2.id = values.id;
            return View();
        }
        // DeleteData = er når man har klikket på "Delete" knappen, koden udf yrer sql commands også for at slet fra database.         
        public IActionResult DeleteData(Delete value)
        {
            int id = value.id;
            SqlConnection Con;

            //fortæller connectionstring.

            Con = new SqlConnection("Data Source=DESKTOP-N0FHQK4; Initial Catalog = LAX; Integrated Security = True");
            //åbner connection

            Con.Open();

            //udfører sql commands

            SqlCommand adp2 = new SqlCommand("DELETE FROM film WHERE id='" + id + "'", Con);
            adp2.ExecuteNonQuery();

            SqlCommand adp = new SqlCommand("DELETE FROM instruktør WHERE id= '" + id + "'", Con);
            adp.ExecuteNonQuery();             
            // Lukker forbindelsen.
            Con.Close();

            //fætter den skal gå til index (film liste).
            return RedirectToAction("index");
        }
        // Edit (og EditData) = Bruger vi til at redigere, lidt samme stil som Delete & DeleteData, hvor EditData er efter knaptryk         
        public IActionResult Edit(Movie values, Edit values2)
        {

            values2.id = values.id;
            return View();
        }

        // EditData = Som beskrevet ovenover, er det når vi trykker på edit k nappen(blå).
        public IActionResult EditData(Edit values2)
        {
            // Sætter variabler
            int id = values2.id;
            string FT = values2.Film_titel;
            string Navn = values2.Instruktør_navn;
            int Å = values2.Årstal;

            //Lave try catch for at undgå systemCrash.
            try
            {
                //Fortæller Connection (Connectionstring)
                SqlConnection Con;
                Con = new SqlConnection("Data Source=DESKTOPN0FHQK4; Initial Catalog = LAX; Integrated Security = True");
                Con.Open();

                //Udfører SQL Connection.

                SqlCommand Ok = new SqlCommand("UPDATE film SET Film_titel='" + FT + "', Årstal= '" + Å + "' WHERE id =" + id + "", Con);
                Ok.ExecuteNonQuery();

                SqlCommand Ok1 = new SqlCommand("UPDATE instruktør SET Instruktør_navn= '" + Navn + "' WHERE id =" + id + "", Con);
                Ok1.ExecuteNonQuery();

                Con.Close();
                // Omdirigerer til index (film liste)                 
                return RedirectToAction("index");
            }
            catch (Exception)
            {
                //error - omdirigerer til index med det samme.
                return RedirectToAction("index");
            }



        }

        //Details = Hvor vi kan se detaljer om filmen man har valgt.         
        public IActionResult Details(Movie values, Details values2)
        {
            return View();
        }
        // Vi laver en liste som skal være vores film liste fra sql databasen
        public static List<Movie> list()
        {
            // laver en liste fra Movie modellen
            List<Movie> Mlist = new List<Movie>();
            string connection = "Data Source=DESKTOP-N0FHQK4;Initial Catalog=LAX;Integrated Security=True";
            using (SqlConnection sqlcon = new SqlConnection(connection))
            {
                using (SqlCommand sqlcmd = new SqlCommand("SELECT film.id, fi lm.Film_titel, instruktør.Instruktør_navn, film.Årstal FROM instruktør FULL JOIN film ON instruktør.id = film.id"))
                {
                    using (SqlDataAdapter sqlDA = new SqlDataAdapter())
                    {
                        sqlcmd.Connection = sqlcon;

                        sqlcon.Open();
                        sqlDA.SelectCommand = sqlcmd;
                        SqlDataReader SDR = sqlcmd.ExecuteReader();
                        while (SDR.Read())
                        {
                            //Læser og tilføjer til Movie.
                            Movie Mov = new Movie();
                            Mov.Film_titel = SDR["Film_titel"].ToString();
                            Mov.Instruktør_navn = SDR["Instruktør_navn"].ToString();
                            Mov.Årstal = Convert.ToInt32(SDR["Årstal"]);
                            Mov.id = Convert.ToInt32(SDR["id"]);
                            //Tilføjer til listen                             
                            Mlist.Add(Mov);
                        }
                    }
                    return (Mlist);
                }
            }
        }
    }
}