using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebASP.Models
{
    //ny class Details med propperties til de forskellige attributter
    public class Details
    {
        public string Film_titel { get; set; }
        public string Instruktør_navn { get; set; }
        public int Årstal { get; set; }
    }
}
