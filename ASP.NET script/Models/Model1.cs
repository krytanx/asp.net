using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebASP.Models
{
    //ny class Create med propperties til de forskellige attributter
    public class Create
    {
        public string Instruktør_navn { get; set; }
        public int Årstal { get; set; }

        public string Film_titel { get; set; }
    }
}
