using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebASP.Models
{
    //ny class Instruktør med propperties til de forskellige attributter
    public class Instruktør
    {
        //gør det til unique
        [Key]

        public int id { get; set; }
        public string instruktør_navn { get; set; }
    }
}
