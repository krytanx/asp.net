using System;
using System.Collections.Generic;
using System.Linq; 
using System.Text;
using System.Threading.Tasks;
using System.Windows; 
using System.Windows.Controls; 
using System.Windows.Data; 
using System.Windows.Documents; 
using System.Windows.Input; 
using System.Windows.Media;
using System.Windows.Media.Imaging; 
using System.Windows.Media.Animation; 
using System.Windows.Navigation; 
using System.Windows.Shapes;
 
namespace lax_AS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>     
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();             
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //sætter startop lokalikation på vinduet
            loadprogressbar();//kalder metode loadprogressbar  
            
        }
 
        //metode
        private void loadprogressbar()
        {
            Duration dur = new Duration(TimeSpan.FromSeconds(5));
            // Fortæller den skal fylde (køre) til værdien 100 i progressbare n.
            DoubleAnimation dblani = new DoubleAnimation(100, dur);
            // Starter progressbar animationen             
            pb.BeginAnimation(ProgressBar.ValueProperty, dblani);
 
        }
 
        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {             
            //fortæller hvis dens value er 100 skal den åbne filmbase vinduet og lukke dett e.
            if (pb.Value == 100)
            {
                filmbase open = new filmbase();
                open.Show();
                this.Close();
            }
        }
    }
}
