using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lax_AS
{
    /// <summary>
    /// Interaction logic for Detaljer.xaml
    /// </summary>     
    public partial class Detaljer : Window
    {

        public Detaljer(string title, string Inavn, int årtal)
        {
            FilmBox.Text = title;
            InstruktørBox.Text = Inavn;
            ÅrstalBox.Text = årtal.ToString();

            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //sætter startop lokalikation på vinduet
        }

        private void FilmBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }


        private void InstruktørBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void ÅrstalBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void OkKnap_Click(object sender, RoutedEventArgs e)
        {
            filmbase fbse = new filmbase();
            fbse.Show();
            this.Close();
        }
    }
}
