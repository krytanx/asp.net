using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace lax_AS
{
    /// <summary>
    /// Interaction logic for filmbase.xaml
    /// </summary>     
    public partial class filmbase : Window
    {
        public filmbase()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //sætter startop lokalikation på vinduet
            Load();
        }


        //Load metode
        public void Load()
        {
            //sætter sql connection til at være Con,
            SqlConnection Con;
            //fortæller connectionstring.
            Con = new SqlConnection("Data Source=DESKTOP-N0FHQK4;Initial Catalog=LAX;Integrated Security=True");
            //åbner connection
            Con.Open();

            //Udfører SQL Connection.
            SqlDataAdapter adp = new SqlDataAdapter("SELECT Film_titel, Instr uktør_navn, Årstal FROM instruktør, film WHERE Film.id = instruktør.id ORDER BY Instruktør_navn", Con);
            // laver nyt datatTable, fortæller den skal fyldes med informationerne fra adp.
            DataTable dt = new DataTable();
            adp.Fill(dt);
            dgrid.ItemsSource = dt.DefaultView;
            Con.Close();


        }

        //Slet knap på datagrid.
        private void Slet(object sender, RoutedEventArgs e)
        {
            try
            {
                string id = "";
                int finalID = 0;

                SqlConnection Con;
                //fortæller connectionstring.
                Con = new SqlConnection("Data Source=DESKTOPN0FHQK4;Initial Catalog=LAX;Integrated Security=True");
                //åbner connection                 
                Con.Open();

                /////////////////////////////////////////////
                var cellInfos = dgrid.SelectedCells;

                var list1 = new List<string>();
                string filmtitel = "", instruktørNavn = "";
                int årstal = 0;

                foreach (DataGridCellInfo cellInfo in cellInfos)
                {

                    if (cellInfo.IsValid)
                    {

                        //GetCellContent returns FrameworkElement
                        var content = cellInfo.Column.GetCellContent(cellInfo.Item);

                        //Need to add the extra lines of code below to get desired output
                        //get the datacontext from FrameworkElement and typecast to DataRowView                         
                        var row = (DataRowView)content.DataContext;
                        //ItemArray returns an object array with single element                         
                        object[] obj = row.Row.ItemArray;
                        //store the obj array in a list or Arraylist for later use                         
                        list1.Add(obj[0].ToString());
                        filmtitel = obj[0].ToString();
                        instruktørNavn = obj[1].ToString();
                        årstal = Convert.ToInt32(obj[2].ToString());
                        //ListOBJ 1 = Filmtitel
                        //ListOBJ 2 = Instruktørnavn
                        //ListOBJ 3 = årstal
                    }
                }

                /////////////////////////////////////////////


                //er finder den ud af hvilken "row" den er på(nummer) så istedet for 0,1,2,3 så pga. tal=1 vil index hede 1,2,3,4 ect.
                // int tal = 1;
                // var row = GetParent<DataGridRow>((Button)sender);
                // var index = dgrid.Items.IndexOf(row.Item);
                // tal = index;
                // tal = tal + 1;


                Con.Close();
                SqlCommand SelectCommand = new SqlCommand("SELECT id FROM fil m WHERE Film_titel ='" + filmtitel + "'AND årstal='" + årstal + "'", Con);
                SqlDataReader myreader;
                Con.Open();
                myreader = SelectCommand.ExecuteReader();
                string identity = "";
                while (myreader.Read())
                {
                    identity = myreader[0].ToString();
                }
                Con.Close();
                Con.Open();

                //Udfører SQL Connection.
                SqlCommand adp2 = new SqlCommand("DELETE FROM film WHERE id='" + identity + "'", Con);
                adp2.ExecuteNonQuery();
                SqlCommand adp = new SqlCommand("DELETE FROM instruktør WHERE id='" + identity + "'", Con);
                adp.ExecuteNonQuery();
                Con.Close();
                Load();
            }

            catch (Exception)

            {
                MessageBox.Show("Something went wrong!", "Error!");
                filmbase baseF = new filmbase();
                baseF.Show();
                this.Close();
            }
        }

        // Rediger knap.         
        private void Rediger(object sender, RoutedEventArgs e)
        {
            try
            {
                string id = "";
                int finalID = 0;

                SqlConnection Con;
                //fortæller connectionstring.
                Con = new SqlConnection("Data Source=DESKTOPN0FHQK4;Initial Catalog=LAX;Integrated Security=True");
                //åbner connection                 
                Con.Open();

                /////////////////////////////////////////////
                var cellInfos = dgrid.SelectedCells;

                var list1 = new List<string>();
                string filmtitel = "", instruktørNavn = "";
                int årstal = 0;

                foreach (DataGridCellInfo cellInfo in cellInfos)
                {
                    if (cellInfo.IsValid)
                    {
                        //GetCellContent returns FrameworkElement
                        var content = cellInfo.Column.GetCellContent(cellInfo.Item);

                        //Need to add the extra lines of code below to get desired output  
                        //get the datacontext from FrameworkElement and typecast to DataRowView                         
                        var row = (DataRowView)content.DataContext;
                        //ItemArray returns an object array with single element                         
                        object[] obj = row.Row.ItemArray;
                        //store the obj array in a list or Arraylist for later use                         
                        list1.Add(obj[0].ToString());
                        filmtitel = obj[0].ToString();
                        instruktørNavn = obj[1].ToString();
                        årstal = Convert.ToInt32(obj[2].ToString());
                        //ListOBJ 1 = Filmtitel
                        //ListOBJ 2 = Instruktørnavn
                        //ListOBJ 3 = årstal
                    }
                }

                /////////////////////////////////////////////


                //er finder den ud af hvilken "row" den er på(nummer) så istedet for 0, 1, 2, 3 såpga.tal = 1 vil index hede 1, 2, 3, 4 ect.
                // int tal = 1;
                // var row = GetParent<DataGridRow>((Button)sender);
                // var index = dgrid.Items.IndexOf(row.Item);
                // tal = index;
                // tal = tal + 1;


                Con.Close();
                SqlCommand SelectCommand = new SqlCommand("SELECT id FROM fil m WHERE Film_titel ='" + filmtitel + "'AND årstal='" + årstal + "'", Con);
                SqlDataReader myreader; 
                Con.Open();

                myreader = SelectCommand.ExecuteReader();

                string identity = "";

                while (myreader.Read())
                {
                    identity = myreader[0].ToString();
                }
                Con.Close();

                Rediger redi = new Rediger(identity);
                redi.Show();
                this.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong!", "Error!");
                filmbase baseB = new filmbase();
                baseB.Show();
                this.Close();
            }

        }

        //Detalje knappen.
        private void Detalje(object sender, RoutedEventArgs e)
        {
            try
            {
                string id = "";
                int finalID = 0;
 
                SqlConnection Con;
                //fortæller connectionstring.
                Con = new SqlConnection("Data Source=DESKTOPN0FHQK4;Initial Catalog=LAX;Integrated Security=True");
                //åbner connection                 Con.Open();
 
                /////////////////////////////////////////////
                var cellInfos = dgrid.SelectedCells;
 
                var list1 = new List<string>();
                string filmtitel = "", instruktørNavn = "";
                int årstal = 0;
 
                foreach (DataGridCellInfo cellInfo in cellInfos)
                {
                    if (cellInfo.IsValid)
                    {
                        //GetCellContent returns FrameworkElement
                        var content = cellInfo.Column.GetCellContent(cellInfo.Item);
 
                        //Need to add the extra lines of code below to get desired output  
                        //get the datacontext from FrameworkElement and typecast to DataRowView                         
                        var row = (DataRowView)content.DataContext;  
                        //ItemArray returns an object array with single element                         
                        object[] obj = row.Row.ItemArray;  
                        //store the obj array in a list or Arraylist for later use                         
                        list1.Add(obj[0].ToString());
                        filmtitel = obj[0].ToString();
                        instruktørNavn = obj[1].ToString();
                        årstal = Convert.ToInt32(obj[2].ToString());
                        MessageBox.Show(filmtitel);
                        //ListOBJ 1 = Filmtitel
                        //ListOBJ 2 = Instruktørnavn
                        //ListOBJ 3 = årstal
 
 
                        Detaljer DeT = new Detaljer(filmtitel, instruktørNavn , årstal);
                        DeT.Show();
                        this.Close();
                    }
                    }
            }
            catch (Exception)
            {
 
                MessageBox.Show("Something went wrong!", "Error!");
                filmbase baseC = new filmbase();
                baseC.Show();
                this.Close();
            }
            
        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Empty
        }
 
        private void Button_Click(object sender, RoutedEventArgs e)         {
            //Empty
        }
 
        private void Opret_Click(object sender, RoutedEventArgs e)         {
            OpretWindow Oprettele = new OpretWindow();
            Oprettele.Show();
            this.Close();
        }
    }
}





