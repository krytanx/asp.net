using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace lax_AS
{
    /// <summary>
    /// Interaction logic for Rediger.xaml
    /// </summary>     
    public partial class Rediger : Window
    {
        public string id2;
        public Rediger(string identity)
        {
            id2 = identity;
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //sætter startop lokalikation på vinduet
        }
        private void OkKnap_Click(object sender, RoutedEventArgs e)
        {
            //sætter sql connection til at være Con,
            SqlConnection Con;

            //fortæller connectionstring.
            Con = new SqlConnection("Data Source=DESKTOP-N0FHQK4;Initial Catalog=LAX;Integrated Security=True");
            Con.Open();
            SqlCommand Ok = new SqlCommand("UPDATE film SET Film_titel ='" + FilmBox.Text + "', Årstal = '" + ÅrstalBox.Text + "' WHERE id =" + id2 + "", Con);
            Ok.ExecuteNonQuery();
            SqlCommand Ok1 = new SqlCommand("UPDATE instruktør SET Instruktør _navn = '" + InstruktørBox.Text + "' WHERE id =" + id2 + "", Con); Ok1.ExecuteNonQuery(); Con.Close();

            MessageBox.Show("Din film blev redigeret!");
            InstruktørBox.Text = "";
            FilmBox.Text = "";
            ÅrstalBox.Text = "";
            filmbase FBase = new filmbase();
            FBase.Show();
            this.Close();
        }

        private void FilmBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void InstruktørBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ÅrstalBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void CancelKnap_Click(object sender, RoutedEventArgs e)
        {
            filmbase filmBS = new filmbase();
            filmBS.Show();
            this.Close();
        }
    }
}


