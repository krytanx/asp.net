using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.IO;

namespace lax_AS
{
    /// <summary>
    /// Interaction logic for OpretWindow.xaml
    /// </summary>     
    public partial class OpretWindow : Window
    {
        public OpretWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            // sætter startop lokalikation på vinduet


        }

        private void OkKnap_Click(object sender, RoutedEventArgs e)
        {
            bool Try = true;
            string id = "";
            int finalID = 0;



            //sætter sql connection til at være Con,
            SqlConnection Con;

            //fortæller connectionstring.
            Con = new SqlConnection("Data Source=DESKTOPN0FHQK4;Initial Catalog=LAX;Integrated Security=True");

            SqlCommand Ok1 = new SqlCommand("SELECT id FROM instruktør", Con);
            try
            {

                Con.Open();

                using (SqlDataReader read = Ok1.ExecuteReader())
                {
                    while (read.Read())
                    {
                        id = (read["id"].ToString());
                    }
                }

                int masterID = Convert.ToInt32(id);
                finalID = masterID + 1;
                Con.Close();
            }
            catch (Exception IDfejl)
            {
                finalID = 1;
                Con.Close();
            }
            try
            {
                Con.Open();
                SqlCommand Ok = new SqlCommand("INSERT INTO instruktør(id, In struktør_navn) VALUES('" + finalID + "','" + InstruktørBox.Text + "' )", Con);
                Ok.ExecuteNonQuery();
                SqlCommand Ok2 = new SqlCommand("INSERT INTO film(id, Film_ti tel, Årstal) VALUES('" + finalID + "','" + FilmBox.Text + "','" + ÅrstalBox.Text + "' )", Con);
                Ok2.ExecuteNonQuery();
                Try = false;
            }
            catch (Exception ImputFailure)
            {

                Try = true;
                MessageBox.Show("Imput var forkert eller tom! prøv igen!", "FEJL!");

            }
            if (Try == false)
            {
                MessageBox.Show("Din film blev oprettet!", "Ok", MessageBoxButton.OK);
            }
            InstruktørBox.Text = "";
            FilmBox.Text = "";
            ÅrstalBox.Text = "";
            filmbase filmA = new filmbase();
            filmA.Show();
            this.Close();
        }

        private void FilmBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void CancelKnap_Click(object sender, RoutedEventArgs e)
        {
            filmbase filmB = new filmbase();
            filmB.Show();
            this.Close();
        }

    }
}
