
use master 
go 
if exists(select * from sys.databases where name = 'LAX')
begin     
	drop database [LAX] 
end 
GO 
create database [LAX] 
go 
USE LAX 
create table instruktør(     
id int NOT NULL PRIMARY KEY,
navn NVARCHAR(50) not NULL

) 

create table film(     
id int not null FOREIGN KEY(id) REFERENCES instruktør(id),
titel nvarchar(50) not null,
årstal int not null CHECK(årstal > 1300 and årstal < YEAR(GETDATE()+1)) 
) 
GO 

INSERT INTO instruktør(navn) VALUES ('Kim') 

INSERT INTO film(id,titel,årstal) VALUES(1,'bu',1400) 

SELECT * FROM film, instruktør 

SELECT titel, navn, årstal FROM film, instruktør
WHERE film.id = instruktør.id 
ORDER BY film.titel, instruktør.navn,film.årstal  